import { NgModule, OnInit, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ViewreportService } from '../common-services/viewreport.service';
import * as CryptoJS from 'crypto-js';
import { Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';
import * as moment from 'moment';

import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { AmpurService } from '../common-services/ampur.service';
import { ClnService } from '../common-services/cln.service';
import { PcuService } from '../common-services/pcu.service';
import { IdpmService } from '../common-services/idpm.service';
import { DctService } from '../common-services/dct.service';

// import { Typeahead } from 'ng2-typeahead';

// @NgModule({
//   declarations: [ Typeahead ],
// })

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    providers: [ViewreportService],
    // styleUrls: ['./view.component.scss']



})

export class ViewComponent implements OnInit {
    errorMessage: string;
    open: boolean = false;
    isUpdate: boolean = false;

    Dataviews: any[] = [];
    AllMenu: any[] = [];

    title_name: any;
    comment: any;
    template: any;

    subitems: any[] = [];

    ampur: any[] = [];
    cln: any[] = [];
    pcu: any[] = [];
    idpm: any[] = [];

    // dctName: string;
    dct: any = [] = [];


    getSubItem: any = [];
    menu_id: any;
    item_id: any;
    sub_id: any;
    sql: any;
    params: any;
    param_x: any = [];
    param_xx: any = [];
    param: any = [];

    rowLength: any;
    date: Date = new Date();
    mode = 'Promise';

    tableDatas: any = [];
    fieldDatas: any = [];
    loading: boolean = false;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private viewreportService: ViewreportService,
        private ampurService: AmpurService,
        private clnService: ClnService,
        private pcuService: PcuService,
        private idpmService: IdpmService,
        private dctService: DctService


    ) {
        this.route.params.subscribe(params => {
            this.sub_id = params['menu_id'];
            this.fieldDatas = [null];
            this.tableDatas = [null];

            // console.log(this.sub_id);
            this.showDatas(this.sub_id);
        })
        // console.log(this.route.params);
    }

    ngOnInit() {
        this.showAmpur();
        this.showCln();
        this.showIdpm();
        this.showDct();
    }

    showAmpur() {
        this.ampur = [];
        this.ampurService.selectAmpur()
            .then((result: any) => {
                if (result.ok) {
                    this.ampur = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    // console.log(this.ampur);
                }
            }).catch(error => {
                console.log(error);

            })

    }

    showCln() {
        this.cln = [];
        this.clnService.selectCln()
            .then((result: any) => {
                if (result.ok) {
                    this.cln = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    // console.log(this.cln);
                }
            }).catch(error => {
                console.log(error);

            })

    }
    showPcu(ampur) {
        this.pcu = [];
        this.pcuService.selectPcu(ampur)
            .then((result: any) => {
                if (result.ok) {
                    this.pcu = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    // console.log(this.pcu);
                }
            }).catch(error => {
                console.log(error);

            })

    }

    showIdpm() {
        this.idpm = [];
        this.idpmService.selectIdpm()
            .then((result: any) => {
                if (result.ok) {
                    this.idpm = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    // console.log(this.idpm);
                }
            }).catch(error => {
                console.log(error);

            })
    }

    showDct() {
        this.dct = [];
        this.dctService.selectDct()
            .then((result: any) => {
                if (result.ok) {
                    this.dct = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    // console.log(this.dct);
                }
            }).catch(error => {
                console.log(error);
            })
    }

    exportToExcel() {
        const options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: [this.fieldDatas]
        };
        // tslint:disable-next-line:no-unused-expression
        new Angular2Csv(this.Dataviews, this.title_name, options);
        // this.excelService.exportAsExcelFile(excelDatas, this.tableName);
    }

    showDatas(sub_id) {
        this.subitems = [];
        this.param_xx = [];
        this.param_x = [null];
        this.param = [null];

        this.viewreportService.selectReport(this.sub_id)
            .then((result: any) => {
                if (result.ok) {
                    this.subitems = result.rows;   // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    this.sql = this.subitems[0].query_sql
                    this.params = this.subitems[0].query_params
                    this.title_name = this.subitems[0].sub_item_name
                    this.comment = this.subitems[0].comment
                    this.template = this.subitems[0].template
                    if (this.params) {
                        this.open = true;
                        this.param_xx = this.params.split(",");
                        //    console.log(this.param_xx);
                    } else {
                        this.open = false;
                    }
                    this.Dataviews = [];
                    this.viewreportService.viewReport(this.sql, this.params)
                        .then((res: any) => {
                            const datas = [];
                            if (res.ok) {
                                const _datafield = [];
                                this.Dataviews = res.rows[0]; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                                this.AllMenu = res.rows[1]; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้

                                _.forEach(this.AllMenu, (v, k) => {  // ดึงข้อมูล colums ไปเก็บไว้ที่ _datafield
                                    _datafield.push(v.name);
                                })
                                this.Dataviews.forEach(v => {   // ดึงข้อมูล roows ไปเก็บไว้ที่ _data
                                    let _data = [];
                                    _.forEach(v, x => {
                                        _data.push(x);
                                    });
                                    this.tableDatas.push(_data);  // ส่งค่า _data ไปเก็บใน this.tableDatas เพื่อไปแสดงหน้า thml
                                });
                                this.fieldDatas = _datafield;  // ส่งค่า _datafield ไปเก็บใน this.fieldDatas เพื่อไปแสดงหน้า thml
                            }

                        }).catch(error => {
                            console.log("eror:" + error);
                        })
                }
            }).catch(error => {
                console.log(error);
            })
    }

    KeyParam(xx, input, idx) {
        let param: any;
        let name: any = xx;
        let data: any = input.value;
        this.param[idx] = { name, data };
    }

    Paramampur(xx, input, idx, param_xx) {
        let param: any;
        let name: any = xx;
        let data: any = input.value;
        this.param[idx] = { name, data };

        if (param_xx = "pcu") {
            this.showPcu(input.value);
            // console.log(input.value);
        }
        // console.log(this.param);
    }

    showParams() {
        this.loading = true;
        this.fieldDatas = [null];
        this.tableDatas = [null];
        let i: any;
        let x: any;
        let xx: any;
        this.open = false;

        for (i = 0; i < this.param.length; i++) {
            x = this.param[i].name;
            xx = this.param[i].data;
            this.param_x[i] = xx;
        }
        this.params = this.param_x;
        this.Dataviews = [];

        this.viewreportService.viewReport(this.sql, this.params)
            .then((res: any) => {
                const datas = [];
                if (res.ok) {
                    // tslint:disable-next-line:no-shadowed-variable
                    const xx = res.rows[0].length
                    const _datafield = [];
                    if (res.rows[1][0] != null) {
                        this.Dataviews = res.rows[0]; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                        this.AllMenu = res.rows[1];
                    } else if (res.rows[0][3] != null) {
                        this.Dataviews = res.rows[0][3]; // ตอนรับ ก็ต้องมารับค่า rows แบบ ตัวแปร 1 แยกออกหลายจุด
                        this.AllMenu = res.rows[1][3];
                    } else {
                        this.Dataviews = res.rows[0][2]; // ตอนรับ ก็ต้องมารับค่า rows แบบ ตัวแปร 1 แยกออกหลายจุด
                        this.AllMenu = res.rows[1][2];
                    }
                    _.forEach(this.AllMenu, (v, k) => {  // ดึงข้อมูล colums ไปเก็บไว้ที่ _datafield
                        _datafield.push(v.name);
                    })
                    this.Dataviews.forEach(v => {   // ดึงข้อมูล roows ไปเก็บไว้ที่ _data
                        let _data = [];
                        // tslint:disable-next-line:no-shadowed-variable
                        _.forEach(v, x => {
                            _data.push(x);
                        });
                        this.tableDatas.push(_data);  // ส่งค่า _data ไปเก็บใน this.tableDatas เพื่อไปแสดงหน้า thml
                    });
                    this.fieldDatas = _datafield;  // ส่งค่า _datafield ไปเก็บใน this.fieldDatas เพื่อไปแสดงหน้า thml
                }
                this.loading = false;
            }).catch(error => {
                console.log(error);
            })
    }
}
