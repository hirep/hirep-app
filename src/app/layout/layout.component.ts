import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params, RoutesRecognized } from '@angular/router';

import { MenuGrpService } from '../common-services/menugrp.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    // styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    getMenuGrp: any = [];
    getMenuTyp: any = [];
    getSubItem: any = [];
    getRout: any = [];
    menu_id: any;
    userFname: string;
    userLevel: any;
    querygroup: any = [];
    // var_menu_id: any;
    i: any;
    item_id: any = [];
    sub_id: any;
    // var_item_id: any;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private menuGrpService: MenuGrpService,
    ) { }

    ngOnInit() {

        this.userFname = sessionStorage.getItem('userFname');
        this.userLevel = sessionStorage.getItem('userLevel');

        this.ShowMenuGroup();
        this.ShowItemAll();
        // console.log(this.userFname);
        // console.log(this.userLevel);
    }
    doLogout() {
        sessionStorage.removeItem('userFname');
        sessionStorage.removeItem('userLevel');
        sessionStorage.removeItem('querygroup');
        location.reload();
    }

    ShowMenuGroup() {
        this.getMenuGrp = [];
        this.menuGrpService.getMenuGrp()
            .then((rows: any) => {
                if (rows.ok) {
                    this.getMenuGrp = rows.rows;
                    // console.log(this.getMenuGrp);
                } else {
                    console.log(JSON.stringify(rows.error));
                }
            })
            .catch(() => {
                console.log("Server Error")
            })
    }
    ShowMenu(menu_id) {
        this.item_id = [null];
        this.menu_id = menu_id;
        // this.userLevel = userLevel;
        this.getMenuTyp = [];

        this.menuGrpService.getMenuItem(this.menu_id)
            .then((rows: any) => {
                if (rows.ok) {
                    this.getMenuTyp = rows.rows;
                    // console.log(this.getMenuTyp);
                    // console.log(this.getMenuTyp.length);


                    for (this.i = 0; this.i < this.getMenuTyp.length; this.i++) {
                        this.item_id[this.i] = this.getMenuTyp[this.i].item_id

                    }

                    // console.log(this.item_id);
                    this.getSubItem = [];
                    this.menuGrpService.getSubItem(this.item_id, this.userLevel)
                        .then((res: any) => {
                            if (rows.ok) {
                                this.getSubItem = res.rows;
                                // console.log(this.getSubItem);
                            } else {
                                console.log(JSON.stringify(res.error));
                            }
                        })
                        .catch(() => {
                            console.log("Server Error")
                        })


                } else {
                    console.log(JSON.stringify(rows.error));
                }
            })
            .catch(() => {
                console.log("Server Error")
            })
    }

    ShowItemAll() {
        this.item_id = [null];
        this.getMenuTyp = [];
        this.querygroup = [];

        this.querygroup = sessionStorage.querygroup;
        // console.log(this.querygroup);

        if (!this.querygroup || this.querygroup === null) {

            // console.log("1");
            this.menuGrpService.getItemAll()
                .then((rows: any) => {
                    if (rows.ok) {
                        this.getMenuTyp = rows.rows;
                        // console.log(this.getMenuTyp);
                        // console.log(this.getMenuTyp.length);
                        for (this.i = 0; this.i < this.getMenuTyp.length; this.i++) {
                            this.item_id[this.i] = this.getMenuTyp[this.i].item_id
                        }
                        // console.log(this.userLevel);
                        // console.log(this.item_id);
                        this.getSubItem = [];
                        this.menuGrpService.getSubItem(this.item_id, this.userLevel)
                            .then((res: any) => {
                                if (rows.ok) {
                                    this.getSubItem = res.rows;
                                    // console.log(this.getSubItem);
                                } else {
                                    console.log(JSON.stringify(res.error));
                                }
                            })
                            .catch(() => {
                                console.log("Server Error")
                            })


                    } else {
                        console.log(JSON.stringify(rows.error));
                    }
                })
                .catch(() => {
                    console.log("Server Error")
                })

        } else {

            // console.log("2");
            this.menuGrpService.getItemgroup(this.querygroup)
                .then((rows: any) => {
                    if (rows.ok) {
                        this.getMenuTyp = rows.rows;
                        // console.log(this.getMenuTyp);
                        // console.log(this.getMenuTyp.length);
                        for (this.i = 0; this.i < this.getMenuTyp.length; this.i++) {
                            this.item_id[this.i] = this.getMenuTyp[this.i].item_id
                        }
                        // console.log(this.userLevel);
                        // console.log(this.item_id);
                        this.getSubItem = [];
                        this.menuGrpService.getSubItem(this.item_id, this.userLevel)
                            .then((res: any) => {
                                if (rows.ok) {
                                    this.getSubItem = res.rows;
                                    // console.log(this.getSubItem);
                                } else {
                                    console.log(JSON.stringify(res.error));
                                }
                            })
                            .catch(() => {
                                console.log("Server Error")
                            })


                    } else {
                        console.log(JSON.stringify(rows.error));
                    }
                })
                .catch(() => {
                    console.log("Server Error")
                })

        }


    }


}
