import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-layoutadm',
    templateUrl: './layoutadm.component.html',
    // styleUrls: ['./layoutadm.component.scss']
})
export class LayoutadmComponent implements OnInit {
    user: any[] = [];
    userLevel: any;
    userFname: any;

    constructor(
        private router: Router
    ) { }

    ngOnInit() {
        this.logLogin()
    }

    logLogin() {
        if (sessionStorage.userLevel != null) {
            console.log('login Success!');
            this.userLevel = sessionStorage.userLevel;
            this.userFname = sessionStorage.userFname;
            // this.user = rows.token; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
            console.log(this.userFname);

        } else {
            this.router.navigate(['hirep/login']); // ส่ง Routes ไป client/home
            // console.log('No Token Success!');
        }

    }

    logLoOut() {
        console.log('logout Success!');
        sessionStorage.removeItem('userFname');
        sessionStorage.removeItem('userLevel');
        // location.reload();
        this.router.navigate(['hirep/login']); // ส่ง Routes ไป client/home

    }

}
