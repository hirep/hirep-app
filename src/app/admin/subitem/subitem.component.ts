import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuSubService } from '../common-services/menusub.service';
import { MenuitemService } from '../common-services/menuitem.service';
import { LevelService } from '../common-services/level.service';
import { AlertService } from '../../common-services/alert.service'

@Component({
    selector: 'app-subitem',
    templateUrl: './subitem.component.html',
    providers: [AlertService],
    // styleUrls: ['./subitem.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SubitemComponent implements OnInit {
    allmenuitem: any = [];
    allSubitem: any = [];
    allMainitem: any = [];
    allLevel: any = [];
    selectSub: any = [];
    datas: any = [];
    main_sub: any = [];
    sub_item_id: any;
    item_id: any;
    sub_item_name: any;
    query_sql: any;
    query_params: any;
    template: any;
    comment: any;
    sub_item_status: any;
    level_id: any;
    open: boolean = false;
    isUpdate: boolean = false;
    loading: boolean = false;
    constructor(
        private menuSunService: MenuSubService,
        private menuItemService: MenuitemService,
        private levelService: LevelService,
        private alertService: AlertService

    ) { }

    ngOnInit() {
        this.showAllMenuSub();
        this.showAllMenuitem();
        this.showAllLevel();
        // this.UpMaimitem();
    }
    Open() {
        this.open = true;
        this.isUpdate = false;

        this.item_id = null;
        this.sub_item_name = null;
        this.query_sql = null;
        this.query_params = null;
        this.template = null;
        this.comment = null;
        this.sub_item_status = null;
        this.level_id = null;
    }

    showAllLevel() {
        // cler ค่า
        this.allLevel = [];

        this.levelService.LevelShow()
            .then((result: any) => {
                if (result.ok) {
                    this.allLevel = result.rows;
                    // console.log(this.allLevel);
                } else {
                    console.log(JSON.stringify(result.error));
                }
            })
            .catch(() => {
                console.log("Server Error");
            })

    }

    SelectSub() {
        // cler ค่า
        this.selectSub = [];

        this.menuSunService.SelectSub()
            .then((result: any) => {
                if (result.ok) {
                    this.selectSub = result.rows;
                    console.log(this.selectSub);

                } else {
                    console.log(JSON.stringify(result.error));
                }
            })
            .catch(() => {
                console.log("Server Error");
            })

    }


    ItemUdate() {
        let i: any;
        let x: any;
        this.selectSub = [];
        this.allMainitem = [];

        this.menuSunService.SelectSub()
            .then((result: any) => {
                if (result.ok) {
                    this.selectSub = result.rows;
                    console.log(this.selectSub);

                    for (i = 0; i < this.selectSub.length; i++) {
                        x = this.selectSub[i].main_query_id;
                        console.log(x);

                        this.datas[i] = x;
                    }

                    console.log(this.datas);

                    this.menuSunService.UpMainSub(this.datas)
                        // tslint:disable-next-line:no-shadowed-variable
                        .then((result: any) => {
                            if (result.ok) {
                                this.allMainitem = result.rows;
                                let xx = this.allMainitem.length;
                                // console.log(this.allMainitem);
                                for (i = 0; i < this.allMainitem.length; i++) {
                                    x = this.allMainitem[i];

                                    console.log(x);
                                    let main_query_id = x.sub_item_id;
                                    let item_id = x.item_id;
                                    let sub_item_name = x.sub_item_name;
                                    let query_sql = x.query_sql;
                                    let query_params = x.query_params;
                                    let column_selected = x.column_selected;
                                    let template = x.template;
                                    let comment = x.comment;
                                    let sub_item_status = x.sub_item_status;
                                    let level_id = x.level_id;


                                    this.main_sub[i] = {
                                        main_query_id,
                                        item_id,
                                        sub_item_name,
                                        query_sql,
                                        query_params,
                                        column_selected,
                                        template,
                                        comment,
                                        sub_item_status,
                                        level_id
                                    }
                                }
                                console.log(this.main_sub);

                                this.menuSunService.InsertSub(this.main_sub)
                                    .then((results: any) => {
                                        if (results.ok) {
                                            console.log("เพิ่มข้อมูลสำเร็จ");
                                            // this.showAllMenuSub();

                                            this.alertService.success("เพิ่มข้อมูลสำเร็จ " + xx + " รายการ");
                                            this.showAllMenuSub();

                                        } else {
                                            console.log("เพิ่มข้อมูลไม่สำเร็จ");
                                            this.alertService.error("เพิ่มข้อมูลไม่สำเร็จ");
                                        }
                                    }).catch(() => {
                                        console.log("SERVER ERROR");
                                    })
                            } else {
                                console.log(JSON.stringify(result.error));
                            }
                        })
                        .catch(() => {
                            console.log("Server Error");
                        })

                } else {
                    console.log(JSON.stringify(result.error));
                }
            })
            .catch(() => {
                console.log("Server Error");
            })

    }


    showAllMenuitem() {
        // cler ค่า

        this.loading = true;
      
        this.allmenuitem = [];

        this.menuItemService.getAllMenuitem()
            .then((result: any) => {
                if (result.ok) {
                    this.allmenuitem = result.rows;
                    // console.log(this.allmenuitem);
                } else {
                    console.log(JSON.stringify(result.error));
                }
            })
            .catch(() => {
                console.log("Server Error");
            })

    }

    showAllMenuSub() {
        // cler ค่า
        this.allSubitem = [];

        this.menuSunService.getAllMenuSub()
            .then((result: any) => {
                if (result.ok) {
                    this.allSubitem = result.rows;
                    // console.log(this.allSubitem);
                } else {
                    console.log(JSON.stringify(result.error));
                }
           
                this.loading = false;
            })
            .catch(() => {
                console.log("Server Error");
            })

    }
    addData() {
        console.log(this.item_id);
        console.log(this.sub_item_name);
        console.log(this.query_sql);
        console.log(this.query_params);
        console.log(this.template);
        console.log(this.comment);
        console.log(this.sub_item_status);
        console.log(this.level_id);

        if (this.item_id && this.sub_item_name && this.query_sql) {
            this.menuSunService.addMenuSub(
                this.item_id,
                this.sub_item_name,
                this.query_sql,
                this.query_params,
                this.template,
                this.comment,
                this.sub_item_status,
                this.level_id)
                .then((results: any) => {
                    if (results.ok) {
                        console.log("เพิ่มข้อมูลสำเร็จ");
                        this.showAllMenuSub();
                        this.open = false;
                        this.isUpdate = false;
                        this.item_id = null;
                        this.sub_item_name = null;
                        this.query_sql = null;
                        this.query_params = null;
                        this.template = null;
                        this.comment = null;
                        this.sub_item_status = null;
                        this.level_id = null;
                    } else {
                        console.log("เพิ่มข้อมูลไม่สำเร็จ");
                    }
                }).catch(() => {
                    console.log("SERVER ERROR");
                })

        } else {
            console.log("การกรอกข้อมูล");
        }
    }
    editData(x) {
        console.log(x);
        this.sub_item_id = x.sub_item_id;
        this.item_id = x.item_id;
        this.sub_item_name = x.sub_item_name;
        this.query_sql = x.query_sql;
        this.query_params = x.query_params;
        this.template = x.template;
        this.comment = x.comment;
        this.sub_item_status = x.sub_item_status;
        this.level_id = x.level_id;
        this.isUpdate = true;
        this.open = true;
    }
    updateData() {
        // console.log(this.vardchtype);
        // console.log(this.vardchtypename);
        if (this.item_id && this.sub_item_name && this.query_sql) {
            this.menuSunService.updateMenuSub(
                this.sub_item_id,
                this.item_id,
                this.sub_item_name,
                this.query_sql,
                this.query_params,
                this.template,
                this.comment,
                this.sub_item_status,
                this.level_id)
                .then((results: any) => {
                    if (results.ok) {
                        console.log("แก้ไขข้อมูลเรียบร้อย");
                        this.showAllMenuSub();
                        this.open = false;
                        this.isUpdate = false;

                        this.item_id = null;
                        this.sub_item_name = null;
                        this.query_sql = null;
                        this.query_params = null;
                        this.template = null;
                        this.comment = null;
                        this.sub_item_status = null;
                        this.level_id = null;
                    } else {
                        console.log("แก้ไขข้อมูลไม่สำเร็จ");
                    }
                }).catch(() => {
                    console.log("SERVER ERROR");
                })

        } else {
            console.log("ข้อมูลไม่ครบ");
        }
    }
    save() {
        if (this.isUpdate) {
            this.updateData();

        } else {
            this.addData();
        }

    }
    delete(x) {

        console.log(x);
        this.menuSunService.remove(x.sub_item_id)
            .then((results: any) => {
                if (results.ok) {
                    this.showAllMenuSub();
                } else {
                    console.log(results.error);
                }
            }).catch(() => {
                console.log("SERVER ERROR");
            })
    }

}
