import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LevelService {
    constructor(
        @Inject('API_URL')
        private url: string,
        private http: Http
    ) { }

    LevelShow() {
        return new Promise((resolve, reject) => {
            // route ดูที่ API
            this.http.get(`${this.url}/level`)
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
        })
    }

    addLevel(
        level_id: any,
        level_name: any,
        comment: any,
    ) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.url}/subitems`, {
                level_id: level_id,
                level_name: level_name,
                comment: comment,
            })
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
        })
    }

    updateMenuSub(
        level_id: any,
        level_name: any,
        comment: any,
    ) {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.url}/subitems`, {
                level_id: level_id,
                level_name: level_name,
                comment: comment,
            })
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
        })
    }
    remove(level_id: any) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.url}/subitems/del`, { level_id: level_id })
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
        })
    }
}
