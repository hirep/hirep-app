import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Http } from '@angular/http';
// import { Encrypt } from '../common-services/encrypt';
import { Router } from "@angular/router";
import { UserService } from '../common-services/users.service';
import { AlertService } from '../common-services/alert.service'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    // styleUrls: ['./login.component.scss'],
    providers: [UserService, AlertService],
    styles: ['.error {color:red;}'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
    //  member: any[] = [];
    errorMessage: string;
    status: any[] = [];
    users: any[] = [];
    date: Date = new Date();
    mode = 'Promise';
    username: any;
    password: any;
    adm_status: any;

    constructor(
        private router: Router,
        private userService: UserService,
        // private encryptProvider: Encrypt,
        private alertService: AlertService
    ) { }

    ngOnInit() {

    }
    Login() {
        this.users = [];
        this.userService.Login(this.username, this.password)
            .then((result: any) => {
                if (result.ok) {
                    this.users = result.rows; // ตอนรับ ก็ต้องมารับค่า rows แบบนี้
                    sessionStorage.setItem('login', result.rows); // เก็บค่า Token ไว้ที่เครื่อง Client ไว้
                    sessionStorage.setItem('userFname', this.users[0].fullname);
                    sessionStorage.setItem('userLevel', this.users[0].level_id);
                    sessionStorage.setItem('querygroup', this.users[0].query_group);

                    console.log(sessionStorage.userLevel);
                    console.log(sessionStorage.userFname);

                    if (sessionStorage.userLevel === "4") {
                        this.router.navigate(['admin/submenu']); // ส่ง Routes ไป admin/home
                    } else {
                        this.router.navigate(['/home']); // ส่ง Routes ไป hirep/home
                        // location.reload();
                    }

                    // console.log(this.users);
                    // this.router.navigate(['admin']);
                }
            }).catch(error => {
                console.log(error);
                console.log("Username Or Password Error")
                this.alertService.error("ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง");
                this.username = null;
                this.password = null;
            })
    }
    Cancel() {
        this.router.navigate(['/home']); // ส่ง Routes ไป hirep/home
    }
}
